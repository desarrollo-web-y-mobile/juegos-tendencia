from django.contrib import admin
from django.contrib.auth.models import Group
from .models import List


admin.site.site_header = 'Admin Juegos Tendencia'

class ListAdmin(admin.ModelAdmin):
    exclude = ('title',)

admin.site.register(List, ListAdmin)
admin.site.unregister(Group)
