from django.contrib import admin
from django.urls import include, path
from django.contrib.auth import views as auth_views
from . import vista
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'users', vista.UserViewSet)
router.register(r'groups', vista.GroupViewSet)

urlpatterns = [
    path('admin/', admin.site.urls, name='admin'), 
    path('api/', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('social-auth/', include('social_django.urls', namespace="social")),
    path('',vista.index,name='index'),
    path('informacion.html', vista.informacion, name='informacion'),
    path('galeria.html', vista.galeria, name='galeria'),
    path('cuenta.html', vista.cuenta, name='cuenta'),
    path('login.html',vista.login1,name="login"),
    path('regusr.html',vista.registro,name="regusr"),
    path('logout.html',vista.logout1,name="logout"),

    path('passwdrcv.html',auth_views.PasswordResetView.as_view(template_name="passwdrcv.html"),name="reset_password"),
    path('success.html',auth_views.PasswordResetDoneView.as_view(template_name="success.html"),name="password_reset_done"),
    path('<uidb64>/<token>/changepassword.html',auth_views.PasswordResetConfirmView.as_view(template_name="changepassword.html"), name="password_reset_confirm"),
    path('passdone.html',auth_views.PasswordResetCompleteView.as_view(template_name="passdone.html"),name="password_reset_complete"),
    path('', include('pwa.urls')),
]