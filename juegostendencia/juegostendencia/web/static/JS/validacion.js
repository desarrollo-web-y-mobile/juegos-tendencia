$(function () {

    $("#newModalForm").validate({
        rules: {
            email: {
                required: true,
            },
            password: "required"
        },
        messages: {
            email: {
                required: "* Porfavor ingrese un Email valido"
            },
            password: "* Porfavor ingrese su contraseña"
        }
    });
});

$(function () {

    $("#newModalForm1").validate({
        rules: {
            name:{
                required: true,
                minlenght: 3

            },
            email1: {
                required: true,
            },
            password1: "required"
        },
        messages: {
            name:{
                required: "* Porfavor ingresar un nombre",
                minlenght: "* El nombre debe tener mas de 3 letras"
            },
            email1: {
                required: "* Porfavor ingrese un Email valido"
            },
            password1: "* Porfavor ingrese su contraseña"
        }
    });
});