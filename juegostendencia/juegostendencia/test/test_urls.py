from django.test import SimpleTestCase
from django.urls import reverse, resolve
from juegostendencia.vista import index, informacion, galeria, cuenta

class TestUrls(SimpleTestCase):
    def test_list_index_is_resolved(self):
        url = reverse('index')
        self.assertEquals(resolve(url).func, index)

    def test_list_informacion_is_resolved(self):
        url = reverse('informacion')
        self.assertEquals(resolve(url).func, informacion)

    def test_list_galeria_is_resolved(self):
        url = reverse('galeria')
        self.assertEquals(resolve(url).func, galeria)

    def test_list_cuenta_is_resolved(self):
        url = reverse('cuenta')
        self.assertEquals(resolve(url).func, cuenta)