from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.utils import timezone
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from .forms import RegUsr, LoginForm
from .models import List
from django.core.mail import send_mail
from django.contrib.auth.forms import (
    AuthenticationForm, PasswordChangeForm, PasswordResetForm, SetPasswordForm,
)
from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from rest_framework import permissions
from .serializers import UserSerializer, GroupSerializer

def index(request):
    return render(request, 'index.html',{})

def informacion(request):
    return render(request,'informacion.html',{})

def galeria(request):
    return render(request,'galeria.html',{})

def cuenta(request):
    return render(request,'cuenta.html',{})

#def passwdrcv(response):
 #   form = PasswordResetForm()
  #  return render(response, "passwdrcv.html",{"form":form})

def login1(request):
    form = LoginForm(request.POST or None)
    if form.is_valid():
        data=form.cleaned_data
        user = authenticate(username=data.get("usr"), password=data.get("passwd"))
        if user is not None:
            login(request,user)
            messages.success(request,'Ha iniciado sesion correctamente!')
            return redirect("/")
        else:
            messages.warning(request,'Usuario y/o contraseña incorrectos!')
    return render(request,"login.html", {"form":form})

def logout1(request):
    logout(request)
    messages.warning(request,'Usted ha cerrado sesion!')
    return redirect("/")

def registro(request):
    form1 = RegUsr
    if request.method == "POST":
        form1 = RegUsr(request.POST)

        if form1.is_valid():
            form1.save()
            messages.success(request,'Se ha registrado con exito!')
        return redirect("cuenta.html")
    else:
        form1 = RegUsr()
    return render(request, "regusr.html", {"form1":form1})

class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAuthenticated]


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = [permissions.IsAuthenticated]

