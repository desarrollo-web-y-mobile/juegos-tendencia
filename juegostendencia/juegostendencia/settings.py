from pathlib import Path

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.1/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '@v)h9j3%!j6dmro2kl_mtw2(r6qw5oa^(5#u^h%(wa2n$_%(q%'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ["*"]

STATICFILES_DIRS = ['C:/Users/Harou/Desktop/DUOC/cuarto semestre/desarrollo aplicaciones web y mobile/repositorio/juegos-tendencia/juegostendencia/juegostendencia/web/static']

SOCIAL_AUTH_FACEBOOK_KEY = "3481561828600733"
SOCIAL_AUTH_FACEBOOK_SECRET = "3a405b02b3741a986d57485e81bebcee"

LOGIN_REDIRECT_URL = '/'
# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'juegostendencia',
    'crispy_forms',
    'rest_framework',
    'social_django',
    'pwa',
]



MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'social_django.middleware.SocialAuthExceptionMiddleware',
]

ROOT_URLCONF = 'juegostendencia.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': ['C:/Users/Harou/Desktop/DUOC/cuarto semestre/desarrollo aplicaciones web y mobile/repositorio/juegos-tendencia/juegostendencia/juegostendencia/web'],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'social_django.context_processors.backends',
            ],
        },
    },
]

SOCIAL_AUTH_FACEBOOK_SCOPE = ['email','user_link']

SOCIAL_AUTH_FACEBOOK_PROFILE_EXTRA_PARAMS = {
    'fields': 'id, name, email, picture.type(large), link'
}

SOCIAL_AUTH_FACEBOOK_EXTRA_DATA = [
    ('name','name'),
    ('email','email'),
    ('picture','picture'),
    ('link','profile_url'),
]

WSGI_APPLICATION = 'juegostendencia.wsgi.application'


# Database
# https://docs.djangoproject.com/en/3.1/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR / 'db.sqlite3',
    }
}


# Password validation
# https://docs.djangoproject.com/en/3.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/3.1/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.1/howto/static-files/

STATIC_URL = '/static/'

import os

MEDIA_URL = '/fotospag/'
MEDIA_ROOT = os.path.join(BASE_DIR,"fotospag")

CRISPY_TEMPLATE_PACK="bootstrap4"

#SMTP Configuration

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_POST = '587'
EMAIL_USE_TLS = True
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''

REST_FRAMEWORK = {
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.PageNumberPagination',
    'PAGE_SIZE': 2
}

AUTHENTICATION_BACKENDS = [
    'social_core.backends.facebook.FacebookOAuth2',
    'django.contrib.auth.backends.ModelBackend',
]

PWA_APP_NAME = "JuegosTendencia"
PWA_APP_DESCRIPTION = "Pagina informativa de juegos"
PWA_APP_THEME_COLOR = "#136B5B"
PWA_APP_BACKGROUND_COLOR = "#136B5B"

PWA_APP_ICONS = [
    {
        "src": "/static/fotospag/icono_juegostendencia.png",
        "sizes": "300x300"
    }
]

PWA_APP_ICONS_APPLE = [
    {
        "src": "/static/fotospag/icono_juegostendencia.png",
        "sizes": "300x300"
    }
]

PWA_SERVICE_WORKER_PATH = os.path.join(BASE_DIR,"serviceworker.js")